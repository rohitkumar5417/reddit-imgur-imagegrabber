package rig.main;

import java.awt.color.CMMException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GrabImages {

	public static String projectPath;
	public static void main(String[] args) {


		String[] subreddits = {
				"http://www.reddit.com/r/wallpapers"
				};
		
		projectPath = System.getProperty("user.dir");
		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait= new WebDriverWait(driver,30);
		
		int i = 0;

		while (i < subreddits.length) {
			String folderName = subreddits[i].replace(":", "-").replace("/", "_");
			driver.navigate().to(subreddits[i]);
			driver.manage().window().maximize();
			if (driver.getTitle().equals("reddit.com: over 18?")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[value='yes']")));
				driver.findElement(By.cssSelector("button[value='yes']")).click();
			}

			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("a[href*='imgur.com']")));
			int counter = 0;
			
			int check = i;
			int total = 0;

			while (!driver.findElements(By.cssSelector("a.title.may-blank[href*='i.imgur.com']")).isEmpty() && check == i) {
				total = total + saveImages(driver, folderName);
				counter++;
				if (!driver.findElements(By.cssSelector("a[rel='nofollow next']")).isEmpty())
					driver.findElement(By.cssSelector("a[rel='nofollow next']")).click();
				else
					++i;
			}
			System.out.println("Number of files processed: " + total + " || " + "Subreddit: " + subreddits[i]);
		}
		System.out.println("Exiting");
		driver.close();
		driver.quit();

	}

	private static int saveImages(WebDriver driver, String folderName) {
		List<WebElement> linkElements = driver.findElements(By.cssSelector("a.title.may-blank[href*='i.imgur.com']"));
		List<String> links = new ArrayList<String> ();

		for (int i = 0; i < linkElements.size(); i++) {
			links.add(linkElements.get(i).getAttribute("href"));
			if (!links.isEmpty()){
				String link = links.get(i);
				String[] extension = link.split("\\.");
				String[] filename = link.split("/");
				System.out.println(filename[3] + ": " + (i + 1) + " out of " + linkElements.size());
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HH_mm_ss");
				Date date = new Date();
				try {
					BufferedImage img = ImageIO.read(new URL(links.get(i)));
					File dlFile = new File(projectPath + "/images/" + folderName + "/" + filename[3]);
					dlFile.getParentFile().mkdirs();
					ImageIO.write(img, extension[3], dlFile);
				} catch (IOException | IllegalArgumentException | CMMException | ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();
				}
			}
		}
		return linkElements.size();
	}
}
